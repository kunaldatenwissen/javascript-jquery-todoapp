from django.urls import path
from .views import *

urlpatterns = [
    path('',index,name='index'),
    path('view_save/',view_save,name='view_save'),
    path('item_delete/<int:pk>',item_delete,name='item_delete'),
    path('check_update/',check_update,name='check_update'),
    path('item_update/<int:pk>/<str:item>',item_update,name='item_update'),

]
