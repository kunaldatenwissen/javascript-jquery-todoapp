from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from.models import *
import json
from django.views.decorators.csrf import csrf_exempt
# Create your views here.


csrf_exempt
def index(request):
    return render(request,'index.html')


todo_items=Todo.objects.all()

@csrf_exempt
def view_save(request):
    # todo_items.delete()
    obj_list=[]
    if (request.method == "POST"):
        item=request.POST.get('task')
        print(item)
        data=Todo.objects.create(item=item)
        obj={
            'id':data.id,
            'item':data.item
        }
        obj_list.append(obj)
        print(obj_list)
        

        return JsonResponse({'data':obj_list})

    if (request.method == "GET"):
        items=list(todo_items.values())
        return JsonResponse({'data':items})


@csrf_exempt
def item_delete(request,pk):

    if (request.method == "DELETE"):
        item_delete=todo_items.get(id=pk)
        item_delete.delete()
        return JsonResponse({'data':'successfull'})

 



@csrf_exempt
def item_update(request,pk,item):
    if (request.method == "PUT"):
        item_update=todo_items.get(id=pk)

        item_update.item=item
        item_update.save()
        return JsonResponse({'data':'successfull'})


@csrf_exempt
def check_update(request):
    if (request.method == "POST"):
        check=request.POST.get('number')
        print(check)
        item_update=todo_items.get(id=check)
        if item_update.check == False:
            item_update.check=True
        else:
            item_update.check=False
        item_update.save()

 
        return JsonResponse({'data':'successfull'})
