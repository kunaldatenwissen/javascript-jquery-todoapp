var form = $('#form_id')
form.submit(function () {
    var task = $('#task_id').val();
    var tasks = task.trim(' ', '');
    var numbers = $('#table_body').children().length;
    var number = numbers + 1;
    if (tasks !== '') {
        let text = localStorage.getItem("testJSON");
        var obj_list = []
        var obj = { 'jstask': task, 'number': number, 'check': false }
        if (text !== null) {
            let jsobj = JSON.parse(text);
            for (var i in jsobj) {
                obj_list.push(jsobj[i])
            }
        }
        obj_list.push(obj)
        var myJSON=JSON.stringify(obj_list);
        localStorage.setItem("testJSON", myJSON);
        var add_row = `<tr>
                        <td>
                            <div class="form-check d-flex justify-content-start">
                                <input class="form-check-input check" type="checkbox" value="${number}" id="flexCheckDefault">
                                <input class="form-control remove_border check update ms-2" type="text" value="${obj[i].jstask}" id="${obj[i].number}" >
                            </div>
                        </td>
                        <td class="d-flex justify-content-end" >
                            <button class="btn btn-light delete_row" value="${number}" type="submit">
                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                    viewBox="0 0 32 32">
                                    <title>Delete</title>
                                    <path
                                        d="M30 4h-8v-3c0-0.553-0.447-1-1-1h-10c-0.553 0-1 0.447-1 1v3h-8v2h2v24c0 1.104 0.897 2 2 2h20c1.103 0 2-0.896 2-2v-24h2v-2h-0zM12 2h8v2h-8v-2zM26.002 30l-0.002 1v-1h-20v-24h20v24h0.002z">
                                    </path>
                                </svg>
                            </button>
                        </td>
                    </tr>`
        $('#table_body').append(add_row);
    }
    else {
        alert('Enter the task');
    }
    $('#task_id').val('');
    event.preventDefault();
})
$(document).on('click', '.delete_row', function () {
    var number = $(this).val();
    if (number !== null) {
        var items = JSON.parse(localStorage.getItem('testJSON'));
        for (var i = 0; i < items.length; i++) {
            var object = items[i];
            if (object !== null) {
                if (object.number == number) {
                    console.log(object.number)
                    console.log(items)
                    delete items[i];
                    console.log('list', items)
                }
            }
        }
        item = JSON.stringify(items);
        localStorage.setItem('testJSON', item);
        $(this).parents('tr').remove();
    }
})
$(document).on('click', '.check', function () {
    if ($(this).is(':checked') === true) {
        $(this).parents('tr').attr('class', 'checked');
        $(this).siblings('.check').addClass('checked')
        $(this).siblings('.check').attr('disabled','true')
        var number = $(this).val();
        if (number !== null) {
            var items = JSON.parse(localStorage.getItem('testJSON'));
            for (var i = 0; i < items.length; i++) {
                var object = items[i];
                if (object !== null) {
                    if (object.number == number) {
                        items[i].check = true;
                    }
                }
            }
            item = JSON.stringify(items);
            localStorage.setItem('testJSON', item);
        }
    }
    if ($(this).is(':checked') === false) {
        $(this).parents('tr').removeAttr('class', 'checked');
        $(this).siblings('.checked').removeAttr('disabled')
        $(this).siblings('.check').removeClass('checked')
        var number = $(this).val();
        if (number !== null) {
            var items = JSON.parse(localStorage.getItem('testJSON'));
            for (var i = 0; i < items.length; i++) {
                var object = items[i];
                if (object !== null) {
                    if (object.number == number) {
                        items[i].check = false;
                    }
                }
            }
            item = JSON.stringify(items);
            localStorage.setItem('testJSON', item);
        }
    }
})
let text = localStorage.getItem("testJSON");
let obj = JSON.parse(text);
for (var i in obj) {
    if (obj[i] !== null) {
        if (obj[i].check === false) {
            var rows = `<tr>

            <td>
                <div class="form-check d-flex justify-content-start">
                    <input class="form-check-input check align-self-center " type="checkbox" value="${obj[i].number}" id="flexCheckDefault">
                    <input type="text" class="form-control remove_border ms-2 update check" value="${obj[i].jstask}" id="${obj[i].number}" >
                  
                </div>
            </td>
            <td class="d-flex justify-content-end" >
                <button class="btn btn-light delete_row" value="${obj[i].number}" type="submit">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                        viewBox="0 0 32 32">
                        <title>Delete</title>
                        <path
                            d="M30 4h-8v-3c0-0.553-0.447-1-1-1h-10c-0.553 0-1 0.447-1 1v3h-8v2h2v24c0 1.104 0.897 2 2 2h20c1.103 0 2-0.896 2-2v-24h2v-2h-0zM12 2h8v2h-8v-2zM26.002 30l-0.002 1v-1h-20v-24h20v24h0.002z">
                        </path>
                    </svg>
                </button>
            </td>
            </tr>`
            $('#table_body').append(rows);
        }
        if (obj[i].check === true) {
            var rows = `<tr class="checked " >
            <td>
                <div class="form-check d-flex justify-content-start">
                    <input class="form-check-input check align-self-center " type="checkbox" value="${obj[i].number}" id="flexCheckDefault" checked>
                    <input type="text" class="form-control remove_border ms-2 check checked" value="${obj[i].jstask}" id="${obj[i].number}" disabled >
                </div>
            </td>
            <td class="d-flex justify-content-end" >
                <button class="btn btn-light delete_row" value="${obj[i].number}" type="submit">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                        viewBox="0 0 32 32">
                        <title>Delete</title>
                        <path
                            d="M30 4h-8v-3c0-0.553-0.447-1-1-1h-10c-0.553 0-1 0.447-1 1v3h-8v2h2v24c0 1.104 0.897 2 2 2h20c1.103 0 2-0.896 2-2v-24h2v-2h-0zM12 2h8v2h-8v-2zM26.002 30l-0.002 1v-1h-20v-24h20v24h0.002z">
                        </path>
                    </svg>
                </button>
            </td>
            </tr>`
            $('#table_body').append(rows);
        }
    }
}
$(document).on('change','.update',function(){
    var changed_data=$(this).val()
    var number =$(this).attr('id');
    $(this).val(changed_data)
    var items = JSON.parse(localStorage.getItem('testJSON'));
    for (var i = 0; i < items.length; i++) {
        var object = items[i];
        if (object !== null) {
            if (object.number == number) {  
                items[i].jstask = changed_data;
            }
        }
    }
    item = JSON.stringify(items);
    localStorage.setItem('testJSON', item);
})
